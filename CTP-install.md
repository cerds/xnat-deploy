java -Xmx256m -Xms128m -jar libraries/CTP.jar

http://mircwiki.rsna.org/index.php?title=The_CTP_Launcher_Configuration_Editor


Looking for the installer program file
...found /usr/local/src/CTP-installer.jar
Looking for /CTP/libraries/CTP.jar
Looking for /CTP/libraries/util.jar
Looking for /CTP/libraries/MIRC.jar
...could not find it. [OK, this is a CTP installation]
Finding a directory in which to install the program
...setting the starting directory to /JavaPrograms
...selected directory: /JavaPrograms
Installing /JavaPrograms/CTP/KeyStoreManager.jar
Installing /JavaPrograms/CTP/Launcher.jar
Installing /JavaPrograms/CTP/ROOT/example-index.html
Installing /JavaPrograms/CTP/Runner.jar
Installing /JavaPrograms/CTP/examples/example-config.xml
Installing /JavaPrograms/CTP/examples/example-ctp-dicom-anonymizer.script
Installing /JavaPrograms/CTP/examples/example-dicom-pixel-anonymizer.script
Installing /JavaPrograms/CTP/examples/example-filter.script
Installing /JavaPrograms/CTP/examples/example-storage-index.html
Installing /JavaPrograms/CTP/examples/example-tfs-dicom-anonymizer.script
Installing /JavaPrograms/CTP/examples/example-xml-anonymizer.script
Installing /JavaPrograms/CTP/examples/example-zip-anonymizer.script
Installing /JavaPrograms/CTP/keystore
Installing /JavaPrograms/CTP/libraries/CTP.jar
Installing /JavaPrograms/CTP/libraries/commons-compress-1.0.jar
Installing /JavaPrograms/CTP/libraries/dcm4che-imageio-rle-2.0.25.jar
Installing /JavaPrograms/CTP/libraries/dcm4che.jar
Installing /JavaPrograms/CTP/libraries/edtftpj.jar
Installing /JavaPrograms/CTP/libraries/email/activation.jar
Installing /JavaPrograms/CTP/libraries/email/javax.mail.jar
Installing /JavaPrograms/CTP/libraries/ftp/commons-logging-1.2.jar
Installing /JavaPrograms/CTP/libraries/ftp/commons-net-3.3.jar
Installing /JavaPrograms/CTP/libraries/ftp/commons-vfs2-2.0.jar
Installing /JavaPrograms/CTP/libraries/ftp/jsch-0.1.53.jar
Installing /JavaPrograms/CTP/libraries/getopt.jar
Installing /JavaPrograms/CTP/libraries/imageio/clib_jiio.dll
Installing /JavaPrograms/CTP/libraries/imageio/clib_jiio_sse2.dll
Installing /JavaPrograms/CTP/libraries/imageio/clib_jiio_util.dll
Installing /JavaPrograms/CTP/libraries/imageio/clibwrapper_jiio-1.2-pre-dr-b04.jar
Installing /JavaPrograms/CTP/libraries/imageio/jai_imageio-1.2-pre-dr-b04.jar
Installing /JavaPrograms/CTP/libraries/imageio/libclib_jiio-32.so
Installing /JavaPrograms/CTP/libraries/imageio/libclib_jiio-64.so
Installing /JavaPrograms/CTP/libraries/jdbm.jar
Installing /JavaPrograms/CTP/libraries/log4j.jar
Installing /JavaPrograms/CTP/libraries/pixelmed_codec.jar
Installing /JavaPrograms/CTP/libraries/slf4j-api-1.6.1.jar
Installing /JavaPrograms/CTP/libraries/slf4j-log4j12-1.6.1.jar
Installing /JavaPrograms/CTP/libraries/util.jar
Installing /JavaPrograms/CTP/linux/ctpService-red-old.sh
Installing /JavaPrograms/CTP/linux/ctpService-red.sh
Installing /JavaPrograms/CTP/linux/ctpService-ubuntu.sh
Installing /JavaPrograms/CTP/log4j.properties
Installing /JavaPrograms/CTP/pages/guest-manager.xsl
Installing /JavaPrograms/CTP/pages/list-objects.xsl
Installing /JavaPrograms/CTP/pages/list-studies.xsl
Installing /JavaPrograms/CTP/pages/view-objects.xsl
Installing /JavaPrograms/CTP/profiles/dicom/DICOM-PS3.15-Basic
Installing /JavaPrograms/CTP/profiles/dicom/DICOM-PS3.15-CleanDescriptors
Installing /JavaPrograms/CTP/profiles/dicom/DICOM-PS3.15-CleanGraphics
Installing /JavaPrograms/CTP/profiles/dicom/DICOM-PS3.15-CleanStructuredContent
Installing /JavaPrograms/CTP/profiles/dicom/DICOM-PS3.15-RetainDeviceIDs
Installing /JavaPrograms/CTP/profiles/dicom/DICOM-PS3.15-RetainLongitudinalFullDates
Installing /JavaPrograms/CTP/profiles/dicom/DICOM-PS3.15-RetainLongitudinalModifiedDates
Installing /JavaPrograms/CTP/profiles/dicom/DICOM-PS3.15-RetainPatientCharacteristics
Installing /JavaPrograms/CTP/profiles/dicom/DICOM-PS3.15-RetainUIDs
Installing /JavaPrograms/CTP/profiles/dicom/TCIA-Basic
Installing /JavaPrograms/CTP/profiles/dicom/TCIA-BasicAdditions
Installing /JavaPrograms/CTP/scripts/BurnedInPixelsFilter.script
Installing /JavaPrograms/CTP/scripts/DicomPixelAnonymizer.script
Installing /JavaPrograms/CTP/scripts/DicomServiceAnonymizer.script
Installing /JavaPrograms/CTP/scripts/ForcePatientID.script
Installing /JavaPrograms/CTP/scripts/NBIA.script
Installing /JavaPrograms/CTP/windows/CTP-32.exe
Installing /JavaPrograms/CTP/windows/CTP-64.exe
Installing /JavaPrograms/CTP/windows/CTPw.exe
Installing /JavaPrograms/CTP/windows/install.bat
Installing /JavaPrograms/CTP/windows/uninstall.bat
67 files were installed.
Windows service installer:
...file: /JavaPrograms/CTP/windows/install.bat
...home: /JavaPrograms/CTP
...Service runner copied for 64 bits.
...CTP_HOME: /JavaPrograms/CTP
...JAVA_BIN: /usr/lib/jvm/java-8-openjdk-amd64/jre/bin
Linux service installer:
...file: /JavaPrograms/CTP/linux/ctpService-ubuntu.sh
Looking for /config/config.xml
...setting the port to 1080
ImageIO Tools are already installed, deleting /JavaPrograms/CTP/libraries/imageio
Installation complete.
