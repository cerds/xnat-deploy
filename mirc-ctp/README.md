# Clinical Trials Processor (CTP)

## State!

### [Apache JDBM](https://github.com/jankotek/JDBM3)

* State location set with Pipeline `index` parameter. Common implementation is to place state into the same parent as the root data directory.
  * Things to watch:
    1. Naming conflicts if allocating a common index path for JDBM files. Different pipeline stages must have unique PATH (indexPath) for index parameter!
       * e.g., QuarantineIndex is the hardcoded database name for the Quarantine pipeline stage.
    2. Scale horizontal
    3. Access Iops, running over network storage? Storage of small data (8MB hard limit, 16b recommended, 32KB max.)
    4. Concurrent access, potential corruption.
    5. Must have write access by the user account running CTP

## Notes...

### ImageIO

Deprecation of Java Extension Directories, ImageIO legacy implementation no longer required or available.
* [Oracle blog: Removal of Extension Directories](https://blogs.oracle.com/java-platform-group/planning-safe-removal-of-under-used-endorsed-extension-directories)

The following files that where previously added via Extension Directories (now deprecated and removed) are bundled with the CTP implementation (recommended practice).

ImageIO files previously downloaded from [mirc.rsna.org](http://mirc.rsna.org/ImageIO/linux-x86_64.zip).
* clibwrapper_jiio-1.2-pre-dr-b04.jar
* jai_imageio-1.2-pre-dr-b04.jar
* libclib_jiio.so

```console
/JavaPrograms/CTP/libraries/imageio/clibwrapper_jiio-1.2-pre-dr-b04.jar
/JavaPrograms/CTP/libraries/imageio/jai_imageio-1.2-pre-dr-b04.jar
/JavaPrograms/CTP/libraries/imageio/libclib_jiio-64.so
/JavaPrograms/CTP/libraries/imageio/libclib_jiio-32.so
```
