#!/usr/bin/env bash
# https://wiki.xnat.org/documentation/getting-started-with-xnat/xnat-installation-guide
set -e
#echo "Set disable_coredump false" |sudo tee -a /etc/sudo.conf

function cleanup () {
  [ -d /data/xnat ] && sudo chown -R tomcat7 /data/xnat
  [ -d /data ] && sudo chown root /data
}
trap cleanup EXIT

# Sanity checks and prereqs
# XNAT data folder
sudo mkdir -p /data/xnat/{archive,build,cache,ftp,prearchive,pipeline}
sudo chown -R tomcat7 /data/xnat

#sudo su - postgres -c "createuser -D xnat"
#sudo su - postgres -c "createdb -O xnat xnat"
#sudo su - postgres -c "psql -c \"ALTER USER xnat WITH PASSWORD 'xnat'\""

# XNAT home folder
mkdir -p /data/xnat/home/{config,logs,plugins,work}
sudo chown -R $USER /data

cat |sudo -u tomcat7 tee -a /opt/tomcat7/bin/setenv.sh <<EOF
CATALINA_OPTS="$CATALINA_OPTS -Dxnat.home=/data/xnat/home"
EOF

cat >/data/xnat/home/config/xnat-conf.properties <<EOL
#
# xnat-conf.properties
# XNAT http://www.xnat.org
# Copyright (c) 2016, Washington University School of Medicine
# All Rights Reserved
#
# Released under the Simplified BSD.
#
datasource.driver=org.postgresql.Driver
datasource.url=jdbc:postgresql://localhost/xnat
datasource.username=xnat
datasource.password=xnat

hibernate.dialect=org.hibernate.dialect.PostgreSQL9Dialect
hibernate.hbm2ddl.auto=update
hibernate.show_sql=false
hibernate.cache.use_second_level_cache=true
hibernate.cache.use_query_cache=true
EOL

if [ -f /tmp/xnat-web-1.7.4.1.war ]; then
  sudo mv /tmp/xnat-web-1.7.4.1.war /opt/tomcat7/webapps/ROOT.war
  sudo chown tomcat7 /opt/tomcat7/webapps/ROOT.war
else
  sudo -u tomcat7 curl -LsS -o/opt/tomcat7/webapps/ROOT.war https://api.bitbucket.org/2.0/repositories/xnatdev/xnat-web/downloads/xnat-web-1.7.4.1.war
fi

