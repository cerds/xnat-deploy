#!/usr/bin/env bash
# https://wiki.xnat.org/documentation/getting-started-with-xnat/xnat-installation-guide
set -e
#echo "Set disable_coredump false" |sudo tee -a /etc/sudo.conf

function cleanup () {
  sudo rm -rf /tmp/apache-tomcat-*.tar.gz
  sudo chown root /opt
  sudo chown -R tomcat7 /opt/tomcat7
}
trap cleanup EXIT

sudo apt-get update && sudo apt-get -y install \
  openjdk-8-jdk-headless \
  && sudo apt-get -y clean

# Sanity checks and prereqs
# Create Tomcat system user (tomcat:x:996:996:Apache Tomcat:/:/usr/sbin/nologin)
sudo useradd -c 'Apache Tomcat 7' -d / -M -r -s /usr/sbin/nologin -U tomcat7

sudo chown $USER /opt
[ -f /tmp/apache-tomcat-7.0.103.tar.gz ] || curl -LsS -o/tmp/apache-tomcat-7.0.103.tar.gz https://archive.apache.org/dist/tomcat/tomcat-7/v7.0.103/bin/apache-tomcat-7.0.103.tar.gz
tar xzf /tmp/apache-tomcat-7.0.103.tar.gz -C/opt

sudo chown -R tomcat7 /opt/apache-tomcat-7.0.103
ln -rs /opt/apache-tomcat-7.0.103 /opt/tomcat7

#export CATALINA_HOME='/opt/tomcat7'
#sudo -u tomcat7 -E /opt/tomcat7/bin/startup.sh
# http://10.38.78.34:8080/

