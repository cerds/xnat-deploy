# Ansible playbooks for AIS contributions

NB: Many of the playbooks will require elevated privileges to install software. For this to work with Ansible the user account used to run the Playbook must be able to utilise sudo without the need to enter a password.
There are a number of ways this can be accomplished, but as the Playbook(s) are designed to be run locally to configure your user account, utilising the sudo authentication cache would be preferred.
To ensure that the authentication cache is active run `sudo ls`, then run the playbook.
