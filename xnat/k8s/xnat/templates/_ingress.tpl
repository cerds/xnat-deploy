{{- define "mychart.labels" }}
  annotations:
    nginx.ingress.kubernetes.io/proxy-connect-timeout: "150"
    nginx.ingress.kubernetes.io/proxy-send-timeout: "100"
    nginx.ingress.kubernetes.io/proxy-read-timeout: "100"
    nginx.ingress.kubernetes.io/proxy-buffers-number: "4"
    nginx.ingress.kubernetes.io/proxy-buffer-size: "32k"
{{- end }}
