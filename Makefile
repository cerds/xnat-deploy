all : state/mirc-ctp-gui state/mirc-ctp state/xnat-all

xnat_web_ver = 1.7.6

.PHONY: tomcat-build-prep
tomcat-build-prep : src/apache-tomcat-7.0.103.tar.gz

.PHONY: xnat-build-prep
xnat-build-prep : src/xnat-web-$(xnat_web_ver).war src/xnat-pipeline-engine src/openid-auth-plugin-1.0.0.jar src/xnat-ldap-auth-plugin-1.0.0.jar

.PHONY: xnat-build-docker
xnat-build-docker : xnat-build-prep
	packer build -only=docker xnat.json

src/openid-auth-plugin-1.0.0.jar:
	wget -P./src 'http://dev.redboxresearchdata.com.au/nexus/service/local/repositories/snapshots/content/au/edu/qcif/xnat/openid/openid-auth-plugin/1.0.0-SNAPSHOT/openid-auth-plugin-1.0.0-20190409.122010-10.jar'
	#curl -L -o./src/openid-auth-plugin-1.0.0.jar 'http://dev.redboxresearchdata.com.au/nexus/service/local/artifact/maven/redirect?r=snapshots&g=au.edu.qcif.xnat.openid&a=openid-auth-plugin&v=LATEST&e=jar'

src/xnat-ldap-auth-plugin-1.0.0.jar:
	wget -P./src 'https://bitbucket.org/xnatx/ldap-auth-plugin/downloads/xnat-ldap-auth-plugin-1.0.0.jar'
	#curl -L -o./src/xnat-ldap-auth-plugin-1.0.0.jar https://bitbucket.org/xnatx/ldap-auth-plugin/downloads/xnat-ldap-auth-plugin-1.0.0.jar

state/mirc-ctp-gui : mirc-ctp-gui.json src/CTP.tar.gz src/linux-x86_64
	packer build mirc-ctp-gui.json && touch state/mirc-ctp-gui

state/mirc-ctp : mirc-ctp.json mirc-ctp.service src/CTP.tar.gz src/linux-x86_64 nocloud.iso
	packer build mirc-ctp.json && touch state/mirc-ctp

nocloud.iso : user-data meta-data
	genisoimage -output nocloud.iso -volid cidata -joliet -rock -input-charset utf-8 user-data meta-data

src/CTP-installer.jar :
	curl -L -o./src/CTP-installer.jar http://mirc.rsna.org/download/CTP-installer.jar

src/linux-x86_64 : src/ImageIO_linux-x86_64.zip
	unzip -d ./src ./src/ImageIO_linux-x86_64.zip

src/ImageIO_linux-x86_64.zip :
	curl -L -o./src/ImageIO_linux-x86_64.zip http://mirc.rsna.org/ImageIO/linux-x86_64.zip

state/xnat-all : xnat-all.json state/xnat
	packer build xnat-all.json && touch state/xnat-all

state/xnat : xnat.json src/xnat-web-$(xnat_web_ver).war state/tomcat7
	packer build xnat.json && touch state/xnat

src/xnat-pipeline-engine :
	git clone https://www.github.com/nrgXnat/xnat-pipeline-engine.git src/xnat-pipeline-engine

src/xnat-web-$(xnat_web_ver).war :
	curl -L -o./src/xnat-web-$(xnat_web_ver).war https://api.bitbucket.org/2.0/repositories/xnatdev/xnat-web/downloads/xnat-web-$(xnat_web_ver).war

state/tomcat7 : tomcat7.json src/apache-tomcat-7.0.103.tar.gz tomcat/tomcat.service
	packer build tomcat7.json && touch state/tomcat7

src/apache-tomcat-7.0.103.tar.gz :
	curl -L -o./src/apache-tomcat-7.0.103.tar.gz https://archive.apache.org/dist/tomcat/tomcat-7/v7.0.103/bin/apache-tomcat-7.0.103.tar.gz

.PHONY: ctp-prep
ctp-prep : src/CTP-installer.jar src/linux-x86_64
	packer build mirc-ctp-prep.json

src/focal-server-cloudimg-amd64.ova :
	curl -L -o./src/focal-server-cloudimg-amd64.ova https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64.ova

.PHONY: clean
clean :
	rm -rf state/*
	rm -f src/xnat-web-$(xnat_web_ver).war
	rm -rf src/xnat-pipeline-engine
	rm -f src/ImageIO_linux-x86_64.zip
	rm -rf src/linux-x86_64
	rm -f src/apache-tomcat-7.0.103.tar.gz
