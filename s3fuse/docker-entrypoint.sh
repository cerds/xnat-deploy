#!/bin/bash
#S3FS_EMPTYDIR='/vol'
#S3FS_URL='http://minio-1606344459.minio.svc.cluster.local:9000'
#S3FS_OPTIONS='use_path_request_style'
#S3FS_BUCKET01='project1:/some/path'
#S3FS_BUCKET02='project2'
#S3FS_BUCKET03='project3'

passwd_file="${HOME}/.passwd-s3fs"

function cleanup {
for varname in ${!S3FS_BUCKET*}; do
declare -n bucket=$varname
IFS=':' read -r bucket_name bucket_path <<< "${bucket}"
umount "${S3FS_EMPTYDIR}/${bucket_name}" | true
done
}
trap cleanup EXIT
set -e

# Setup
[ -f "${passwd_file}" ] || [ -f "/etc/passwd-s3fs" ] || \
	{ echo "${S3FS_ACCESS_KEY}:${S3FS_SECRET_KEY}" > "${passwd_file}" && chmod 0600 "${passwd_file}"; }

for varname in ${!S3FS_BUCKET*}; do
declare -n bucket=$varname
IFS=':' read -r bucket_name bucket_path <<< "${bucket}"
[ -d "${S3FS_EMPTYDIR}/${bucket_name}" ] || mkdir -p "${S3FS_EMPTYDIR}/${bucket_name}"
echo "s3fs $bucket ${S3FS_EMPTYDIR}/${bucket_name} -o ${S3FS_OPTIONS},url=${S3FS_URL} -f &"
s3fs $bucket ${S3FS_EMPTYDIR}/${bucket_name} -o ${S3FS_OPTIONS},url=${S3FS_URL} -f &
done
tail -f /dev/null
