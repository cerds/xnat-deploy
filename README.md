# Docker image (NB: Not recommended for production; under development)

```bash
docker pull registry.gitlab.com/cerds/xnat-deploy:1.7.4.1
docker run --rm registry.gitlab.com/cerds/xnat-deploy:1.7.4.1 bin/catalina.sh run
```

Prerequisites
* PostgreSQL database

## Environment variables

* POSTGRES_HOST
* POSTGRES_DB
* POSTGRES_USER
* POSTGRES_PASSWORD

# XNAT LXD/LXC deployment

ref.
* https://linuxcontainers.org/lxd/getting-started-cli/

These instructions have been written for Ubuntu Focal systems. If you are using another distribution then there will be some variation to the steps.

```bash
# Install ZFS modules and tools if this is your prefered file system
sudo apt update
sudo apt -y install zfs-dkms

# Install and configure LXD/LXC
sudo snap install lxd
sudo lxd init	# Answer the interactive queries

# Configure remote image repository and launch XNAT
lxc remote add cerds 146.118.70.40 --public
lxc image list cerds:
lxc launch cerds:mirc-ctp
lxc launch cerds:xnat-1.7.4.1-all	# This image contains XNAT, PostgreSQL and a GUI
```

# XNAT build automation

The build automation process uses Make and Packer to produce an XNAT build chain.

# ISSUES

* VirtualBox freeze when starting Ubuntu images. Ubuntu upstream images require access to a serial "/dec/ttyS0" port which does not automatically configure in VirtualBox. This results in the VirtualBox service freezing while the VM boot process is stalled. Most provisioners will allow modification of the VM prior to the startup process, however Packer does not. The default Vagrant box Vagrantfile needs to be modified for Ubuntu "~/.vagrant.d/boxes/ubuntu-VAGRANTSLASH-focal64/20200907.0.0/virtualbox/Vagrantfile" and a serial port needs to be configured.

```
# Front load the includes
include_vagrantfile = File.expand_path("../include/_Vagrantfile", __FILE__)
load include_vagrantfile if File.exist?(include_vagrantfile)

Vagrant.configure("2") do |config|
  config.vm.base_mac = "020AAEBB63FC"

  config.vm.provider "virtualbox" do |vb|
     vb.customize [ "modifyvm", :id, "--uart1", "0x3F8", "4" ]
# Creating a console log file is not an expected behavior for vagrant boxes. LP #1777827
     vb.customize [ "modifyvm", :id, "--uartmode1", "file", File.join(Dir.pwd, "ubuntu-focal-20.04-cloudimg-console.log") ]
  end
end
```

# Prerequisites for a build environment

* GNU make
* Packer <https://github.com/hashicorp/packer/>
* Docker engine <https://docs.docker.com/get-docker/>
* LXD <https://linuxcontainers.org/lxd/introduction/>

# Instructions

```bash
git clone https://gitlab.com/cerds/xnat-deploy.git
cd xnat-deploy
```

## Build XNAT

```bash
# Download pre-requisite packages, software and repositories
make xnat-build-prep

# Docker image build
packer build -only=docker xnat.json

# LXD image build
make tomcat-build-prep
packer build tomcat7.json
packer build -only=lxd xnat.json
```

# General notes

Run Tomcat from the command line as well as with debug options

```bash
CATALINA_OPTS="${CATALINA_OPTS} -Dxnat.home=/data/xnat/home" sudo -E -u tomcat7 /opt/tomcat7/bin/startup.sh
CATALINA_OPTS="${CATALINA_OPTS} -Dxnat.home=/data/xnat/home -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=8000" sudo -E -u tomcat7 /opt/tomcat7/bin/startup.sh
```

Manually set up PostgreSQL with default XNAT username and password. *NB: NOT FOR PRODUCTION*

```bash
sudo su - postgres -c "createuser -D xnat"
sudo su - postgres -c "createdb -O xnat xnat"
sudo su - postgres -c "psql -c \"ALTER USER xnat WITH PASSWORD 'xnat'\""
```
