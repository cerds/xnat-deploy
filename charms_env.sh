DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
JUJU_REPOSITORY="${DIR}/charms"
LAYER_PATH="${JUJU_REPOSITORY}/layers"
INTERFACE_PATH="${JUJU_REPOSITORY}/interface"
export JUJU_REPOSITORY LAYER_PATH INTERFACE_PATH
