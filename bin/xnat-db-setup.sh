#!/usr/bin/env bash

sudo -u postgres createuser -D xnat
sudo -u postgres createdb -O xnat xnat
sudo -u postgres psql -c "ALTER USER xnat WITH PASSWORD 'xnat'"
