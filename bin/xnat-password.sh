#!/usr/bin/env bash
set -e

XNAT_PASSWORD=$(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c30)

sudo su - postgres -c "psql -c \"ALTER USER xnat WITH PASSWORD '${XNAT_PASSWORD}'\""

sudo sed -i "s/^datasource.password=.*$/datasource.password=${XNAT_PASSWORD}/" /data/xnat/home/config/xnat-conf.properties
